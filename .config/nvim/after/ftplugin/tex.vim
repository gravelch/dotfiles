setlocal autoindent

let g:tex_indent_items=0
let g:tex_indent_and=0
let g:tex_indent_brace=0

set shiftwidth=2
set tabstop=2
