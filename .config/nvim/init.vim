"" Specify a directory for plugin
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin(stdpath('data') . '/plugged')

Plug 'NLKNguyen/papercolor-theme'
Plug 'arzg/vim-corvine'
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'junegunn/fzf.vim'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/goyo.vim'
Plug 'lervag/vimtex'
Plug 'dart-lang/dart-vim-plugin'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'moll/vim-bbye'
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx'] }
Plug 'tyru/sync-term-cwd.vim'
Plug 'tpope/vim-obsession'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'Yggdroot/indentLine'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'vim-test/vim-test'
Plug 'andweeb/presence.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'folke/todo-comments.nvim'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'jvirtanen/vim-hcl'


"" Initialize plugin system
call plug#end()

"" Coc.Nvim
" Coc for lightline
let g:coc_global_extensions = [
    \ 'coc-pyright',
    \ 'coc-css',
    \ 'coc-rls',
    \ 'coc-tsserver',
    "\ 'coc-prettier',
    \ 'coc-json',
    \ 'coc-yaml',
    \ 'coc-flutter',
    \ 'coc-phpls',
    \ 'coc-html',
    "\ 'coc-vetur',
    \ 'coc-go',
    \ 'coc-texlab',
    \ '@yaegassy/coc-volar',
    \ ]
function! CocCurrentFunction()
    return get(b:, 'coc_current_function', '')
endfunction
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction


let test#strategy = "neovim"
let test#go#runner = 'richgo'
let test#go#richgo#options = '-v'

" Set space as leader key
let mapleader="\<Space>"

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()

" Use <cr> to confirm completion
"inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use <Tab> and <S-Tab> to navigate the completion list:<Paste>
"inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <silent><expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
inoremap <silent><expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"

" Use <c-space> to trigger completion.
inoremap <silent><expr> <C-Space> coc#refresh()

" Close the preview window when completion is done.
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

" coc.nvim uses jsonc as configuration file format, highlight comments
autocmd FileType json syntax match Comment +\/\/.\+$+

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

set background=dark
" Use K (uppercase) to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)
nmap <leader>rf :CocCommand workspace.renameCurrentFile<CR>

" Remap for format selected region
xmap <leader>fo  <Plug>(coc-format-selected)
nmap <leader>fo  <Plug>(coc-format-selected)

" enable highlight current symbol on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

"" prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile

"" vim-go
" disable vim-go :GoDef short cut (gd)
" this is handled by LanguageClient [LC]
let g:go_def_mapping_enabled = 0


let g:vimtex_view_method = 'zathura'
let g:vimtex_indent_enabled = 1
let g:vimtex_indent_ignored_envs = ['document', 'verbatim', 'lstlisting', 'frame', 'enumerate']
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_quickfix_mode = 0
let g:vimtex_quickfix_ignore_filters = [
        \ 'Underfull',
        \ 'Overfull',
        \ 'specifier changed to',
        \ 'Token not allowed in a PDF string',
      \ ]
let g:vimtex_complete_enabled = 0
let g:vimtex_syntax_conceal_disable = 1

let g:tex_flavor = 'latex'


"" filetype mapping
let g:coc_filetype_map = {
	\ 'jinja.html': 'html'
	\ }

"" Lightline setting
let g:lightline = {
\   'colorscheme': 'corvine',
\   'active': {
\     'left':[[ 'mode', 'paste' ],
\             [ 'cocstatus', 'currentfunction', 'gitbranch', 'readonly', 'obsess', 'filename', 'modified' ]]
\   },
\   'tab': {
\     'active': ['tabnum'],
\     'inactive': ['tabnum']
\   },
\   'component': {
\     'lineinfo': ' %3l:%-2v',
\   },
\   'component_function': {
\       'cocstatus': 'coc#status',
\       'currentfunction': 'CocCurrentFunction',
\       'gitbranch': 'LightlineFugitive',
\       'obsess': "ObsessionStatus"
\   },
\}
let g:lightline.separator = {
\   'left': '', 'right': ''
\}
let g:lightline.subseparator = {
\   'left': '', 'right': ''
\}
let g:lightline.tabline = {
\   'left': [['buffers']],
\   'right': [['close'],['tabs']]
\}
let g:lightline.component_expand = {
\   'buffers': 'lightline#bufferline#buffers'
\}
let g:lightline.component_type = {
\   'buffers': 'tabsel'
\}

function! LightlineFugitive()
   if exists('*fugitive#Head')
      let branch = fugitive#Head()
      return branch !=# '' ? ' '.branch : ''
   endif
   return ''
endfunction

"" netrw config
let g:netrw_banner = 0
let g:netwr_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_winsize = 20

let g:indentLine_char_list = ['│'] 
let g:indentLine_conceallevel = 2

"" fzf.vim
let g:fzf_preview_window = ['up:40%', 'ctrl-/']
let $FZF_DEFAULT_COMMAND = 'rg --files'

"" presence.nvim
let g:presence_enable_line_number  = 0
let g:presence_main_image          = "file"
let g:presence_editing_text        = "Editing █████"
let g:presence_file_explorer_text  = "Browsing █████"
let g:presence_reading_text        = "Reading █████"
let g:presence_workspace_text      = "Working on █████"
let g:presence_buttons             = 0

" goyo.vim
let g:goyo_width = 50


"" filetype
filetype plugin indent on
set smartindent

"" Line Numbering
set number
set relativenumber

"" path completition
set path+=**
set wildignore+=**/__*

"" Mouse support
set mouse=a

" Bufferline support
set guioptions-=e  
set noshowmode
set showtabline=2

"" Splitting naturally
set splitbelow
set splitright

set hidden

"" Colorscheme
colorscheme corvine
" Transparent termbg
function! AdaptColorscheme()
  highlight clear CursorLine
  highlight Normal ctermbg=none
  highlight LineNr ctermbg=none
  highlight Folded ctermbg=none
  highlight NonText ctermbg=none
  highlight SpecialKey ctermbg=none
"  highlight VertSplit ctermbg=none
  highlight SignColumn ctermbg=none
endfunction
call AdaptColorscheme()

"" Key mapping
nnoremap <Space> <Nop>
nnoremap <silent><Leader><CR> :noh<CR>
nnoremap <silent><Leader>wc :w !detex \| wc -w <CR>
nnoremap <silent><Leader>bb :Buffers<CR>
nnoremap <silent><Leader>bn :bn<CR>
nnoremap <silent><Leader>bp :bp<CR>
nnoremap <silent><Leader>bf :bfirst<CR>
nnoremap <silent><Leader>bl :blast<CR>
nnoremap <silent><Leader>bd :Bdelete<CR>
nnoremap <silent><Leader>co :copen<CR>
nnoremap <silent><Leader>cc :ccl<CR>
nnoremap <silent><Leader>cn :cnext<CR>
nnoremap <silent><Leader>cp :cprevious<CR>
nnoremap <silent><Leader>bD :<c-u>up <bar> %bd <bar> e#<CR>
nnoremap <silent><Leader>ff :Files<CR>
nnoremap <Leader>vo :vertical sfind 

"golang
nnoremap <silent><Leader>gg :GoGenerate<CR>
nnoremap <silent><Leader>gt :GoTest<CR>
" vim-test
nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>

let test#python#runner = 'pytest'
let test#python#pytest#options = '-rP'

" terminal escape
if has('nvim')
  tmap <C-o> <C-\><C-n>
endif

" Disable ex mode
nnoremap Q <nop>

map <silent> <C-n> :NvimTreeToggle<CR>

lua << EOF
vim.g.loaded = 1
vim.g.loaded_netrwPlugin = 1
require("nvim-tree").setup()
require("todo-comments").setup {
	search = {
		command = "rg",
		args = {
			"--color=never",
			"--no-heading",
			"--with-filename",
			"--line-number",
			"--sort", "path",
			"--column",
		},
	}
}
EOF
