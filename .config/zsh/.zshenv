export EDITOR="vim"
export PAGER="less"

export ZGEN_DIR="${ZDOTDIR}/zgen"
export ZGEN_SYSTEM_RECEIPT_F=".config/zsh/zgen/zgen_system_lastupdate"
export ZGEN_PLUGIN_RECEIPT_F=".config/zsh/zgen/zgen_plugin_lastupdate"

## neovim socket for supporting nvr
export NVIM_LISTEN_ADDRESS="/tmp/nvimsocket"
## ruby env
export RBENV_ROOT="${HOME}/.local/lib/rbenv"
## abduco vars 
export ABDUCO_CMD=mtm
## python virtualenv
export WORKON_HOME="${HOME}/.virtualenvs"
export PROJECT_HOME="${HOME}/prj"
## go environments
export GOROOT=/usr/lib/go
export GOPATH="${HOME}/.local/lib/go"
## cargo (rust package manager)
export CARGO_HOME="${HOME}/.local/lib/cargo"
## pub (dart package manager)
export PUB_CACHE="${HOME}/.local/share/pub-cache"

# search (in this case ignore everything in the .git/ folder)
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'

source "${ZDOTDIR}/paths"
source "${ZDOTDIR}/aliases"
source "${ZDOTDIR}/local_aliases"
