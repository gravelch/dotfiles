## ariqfadlan's ZSH config file

fix_cursor() {
  echo -ne '\e[4 q'
}
precmd_functions+=(fix_cursor)

# USD -> RP wrapper using units
function  urp () {
	if [ "$#" -eq 3 ]; then
		units --one-line --compact $1${2:u} ${3:u} | xargs printf "${3:u} %'.2f\n"
	elif [ "$#" -eq 2 ]; then
		units --one-line --compact $1${2:u} IDR | xargs printf "Rp%'.f\n"
	elif [ "$#" -eq 1 ]; then
		units --one-line --compact "$1"USD IDR | xargs printf "Rp%'.f\n"
	else
		print >&2 'USAGE: urp NOMINAL [currency]'
	fi
}

# Changet the behaviour of up and down arrow to complete
# only the typed command, not from the beginning.
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "$key[Up]"   ]] && bindkey -- "$key[Up]"   up-line-or-beginning-search
[[ -n "$key[Down]" ]] && bindkey -- "$key[Down]" down-line-or-beginning-search

# mpv wrapper for playing audio directly from youtube
function yta() {
    mpv --ytdl-format=bestaudio ytdl://ytsearch:"$*"
}

# FZF functions to directly change directory into dirstack
function fzf-cd-dirs()
{
	local dir
	dir=$(dirs -v -l | sed -r 's/[^[:space:]]*[0-9]\t//' 2>/dev/null | fzf --reverse +m) && cd "$dir"
	unset dir
	zle reset-prompt
}
function gi() {curl -sLw "\n" https://www.gitignore.io/api/$@ ;}
_gitignoreio_get_command_list() {
  curl -sL https://www.gitignore.io/api/list | tr "," "\n"
}
_gitignoreio () {
  compset -P '*,'
  compadd -S '' `_gitignoreio_get_command_list`
}
compdef _gitignoreio gi


zle -N fzf-cd-dirs
# bind into ^V
bindkey '\ev' fzf-cd-dirs

function open() {
  xdg-open "$@" 2> /dev/null & disown
}

# mpsyt-wrapper
function mpsyt {
	setopt nullglob
	for file  in /home/"${USER}"/.config/mps-youtube/cache*; do
		rm -f "$file"
	done
	unsetopt nullglob
	/usr/bin/youtube-dl --quiet --rm-cache-dir
	/usr/bin/mpsyt
}
# autocwd vim
source /home/ariqfadlan/.local/share/nvim/plugged/sync-term-cwd.vim/macros

# Python virtualenv
source /usr/bin/virtualenvwrapper_lazy.sh

# FZF power
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# Fix unreadable dirs on NTFS partition
source "${ZDOTDIR}/dircolors"


# Zgen Plugin Manager
if [ `tput colors` = "256" ]; then
    source "${ZGEN_DIR}/zgen.zsh"
    if ! zgen saved; then
	# don't load unecessary module on prezto
	# neccessary to avoid conflict with grml's config
	export ZGEN_PREZTO_LOAD_DEFAULT=0
	# load prezto
	zgen prezto
	# workaround needed to override grml prompt
	zgen load sorin-ionescu/prezto modules/prompt
	# it's nice to have
	zgen load sorin-ionescu/prezto modules/pacman
	# auto update zgen and its packages every ~7 days

	zgen load zdharma/fast-syntax-highlighting
    zgen load zsh-users/zsh-history-substring-search
	# minimal theme
    zgen load subnixr/minimal
    ## save all to init script
	zgen save
    fi
    # fish-like history search
    bindkey -M emacs '^P' history-substring-search-up
    bindkey -M emacs '^N' history-substring-search-down
    # minimal prompt
	MNML_MAGICENTER=(mnml_me_ls mnml_me_git)
fi

# rbenv initialization
eval "$(rbenv init -)"
export PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"

# nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

autoload -U +X bashcompinit && bashcompinit
zmodload -i zsh/parameter
complete -C '/usr/local/bin/aws_completer' aws


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/ariqfadlan/.local/opt/google-cloud-sdk/path.zsh.inc' ]; then . '/home/ariqfadlan/.local/opt/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/ariqfadlan/.local/opt/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/ariqfadlan/.local/opt/google-cloud-sdk/completion.zsh.inc'; fi
