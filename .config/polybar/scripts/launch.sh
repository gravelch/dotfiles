#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

source "${HOME}/.config/polybar/scripts/env.sh"

# Launch bar1 and bar2
if type "xrandr" > /dev/null; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --quiet --reload bar1 &
  done
else
  polybar --reload bar1 &
fi
#polybar bar1 -r &
