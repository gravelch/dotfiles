#!/bin/sh

YAD_WIDTH=200
YAD_HEIGHT=200
BOTTOM=false
#DATE="$(date +"%a %d %H:%M")"

eval "$(xdotool getmouselocation --shell)"

if [ $BOTTOM = true ]; then
    : $(( pos_y = Y - YAD_HEIGHT - 20 ))
    : $(( pos_x = X - (YAD_WIDTH / 2) ))
else
    : $(( pos_y = Y + 20 ))
    : $(( pos_x = X - (YAD_WIDTH) * 13 / 16))
fi

yad --calendar --undecorated --fixed --close-on-unfocus --no-buttons \
    --width=$YAD_WIDTH --height=$YAD_HEIGHT --posx=$pos_x \
	--posy=$pos_y > /dev/null
