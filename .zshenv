export ZDOTDIR="${HOME}/.config/zsh"
export ZGEN_DIR="${ZDOTDIR}/zgen"
export HISTFILE="${ZDOTDIR}/.history"
export DIRSTACKFILE="${ZDOTDIR}/zdirs"
#
## Uniform Qt & GTK+ Theme
export GTK2_RC_FILES="${HOME}/.gtkrc-2.0"
export QT_QPA_PLATFORMTHEME=gtk2
## Clipmenu Environment Variables
export CM_LAUNCHER=dmenu-clipmenu
export CM_DIR=/tmp/clipmenu
export CM_IGNORE_WINDOW=KeePass

source "${ZDOTDIR}"/.zshenv
