# ft='config'
#$pdflatex='pdflatex -interaction=nonstopmode';

$pdf_previewer = "zathura";

$pdf_update_method=0;

$pdf_mode = 1;

$pdflatex="pdflatex --shell-escape %O %S";

$dvi_mode = 0;

$preview_continuous_mode = 1;
