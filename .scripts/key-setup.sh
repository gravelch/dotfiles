#!/bin/sh

MODE=$1

if pgrep sxhkd; then
	killall -r sxhkd
fi

# reset xkeyboard options
setxkbmap -option

# set right alt as compose key
#setxkbmap -option compose:ralt 
if [ "$MODE" = "docked" ]; then
	# docked == true
	setsid sxhkd "$HOME"/.config/sxhkd/docked &
else
	setxkbmap -option altwin:prtsc_rwin 
	setsid sxhkd "$HOME"/.config/sxhkd/laptop &
fi
