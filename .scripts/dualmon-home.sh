#!/bin/sh
INTERN=eDP1
EXTERN1="$( xrandr | grep -w connected | grep -v "${INTERN}" | cut -d ' ' -f 1 | head -n1)"
EXTERN2="$( xrandr | grep -w connected | grep -v "${INTERN}" | cut -d ' ' -f 1 | tail -n1)"
POSITION=$1

reset_gui() {
	feh --bg-fill "$(xdg-user-dir PICTURES)/wall.png"
	~/.config/polybar/scripts/launch.sh
}

if [ -n "${EXTERN2}" ]; then
	if [ "$POSITION" = "dualmix" ]; then
		xrandr --output "${INTERN}" --off \
			--output "${EXTERN1}" \
				--primary --mode 1920x1080 \
				--pos 1080x420 --rotate normal \
			--output "${EXTERN2}" --mode 1920x1080 \
			--pos 0x0 --rotate left
		reset_gui
	elif [ "$POSITION" = "dualvertical" ]; then
		xrandr --output "${INTERN}" --off \
			--output "${EXTERN1}" --mode 1920x1080 --pos 1080x0 --rotate right \
			--output "${EXTERN2}" --primary --mode 1920x1080 --pos 0x0 --rotate left
		reset_gui
	elif [ "$POSITION" = "dualhorizontal" ]; then
		xrandr --output "${INTERN}" --off \
			--output "${EXTERN1}" --primary --mode 1920x1080 --pos 1920x0 --rotate normal \
			--output "${EXTERN2}" --mode 1920x1080 --pos 0x0 --rotate normal
		reset_gui
	fi
fi
