#!/usr/bin/env python3

import gi
gi.require_version('Playerctl', '2.0')
from gi.repository import Playerctl, GLib
from urllib.parse import urlparse
from subprocess import Popen

player = Playerctl.Player()
old_track = ''

def send_notification(info, image, scheme):
    if (scheme == 'file'):
        Popen(['notify-send', '-i', image, info])
    elif (isinstance(scheme, bytes)):
        pass
    else:
        Popen(['notify-send', '-i', 'multimedia-audio-player', info])

def on_track_change(player, e):
    global old_track
    track = '{artist} - {title}'.format(artist=player.get_artist(), title=player.get_title())
    image_path = player.print_metadata_prop('mpris:artUrl')
    scheme = urlparse(image_path).scheme
    if (old_track != track):
        old_track = track
        send_notification(track, image_path, scheme)

def on_start_playing(player, e):
    track = '{artist} - {title}'.format(artist=player.get_artist(), title=player.get_title())
    image_path = player.print_metadata_prop('mpris:artUrl')
    scheme = urlparse(image_path).scheme
    send_notification(track, image_path, scheme)

player.connect('metadata', on_track_change)
player.connect('playback-status::playing', on_start_playing)
GLib.MainLoop().run()
