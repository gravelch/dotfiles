#!/bin/bash

if pgrep -x xidlehook > /dev/null; then
    echo -e "Turning on..."
    killall -r xidlehook
    sleep 3
    xset -dpms
    echo -e "Presentation Mode has been activated..."
else
    echo -e "Turning off..."
    xidlehook --not-when-fullscreen \
	      --not-when-audio \
	      --timer 600 \
		'xset dpms force off' "" \
	      --timer 150 \
		'betterlockscreen -l dimblur' "" \
	      --timer 450 \
		'xset dpms force on && systemctl suspend' "" &
    echo -e "Presentation Mode is inactive now..."
fi
