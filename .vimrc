" gravelch's vimrc
" faster load
set nocompatible


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""	VIMPLUG		  
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
Plug 'junegunn/fzf.vim'
Plug 'godlygeek/tabular'
Plug 'junegunn/goyo.vim'
Plug 'tpope/vim-fugitive'
Plug 'arzg/vim-corvine'
Plug 'aklt/plantuml-syntax'
Plug 'mvanderkamp/worklist.vim'
Plug 'tpope/vim-obsession'

" Initialize plugin system
call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

if has("vms")
  " do not keep a backup file, use versions instead
  set nobackup
else
  " keep a backup file (restore to previous version)
  set backup
  if has('persistent_undo')
	" keep an undo file (undo changes after closing)
    set undofile
  endif
endif

if &t_Co > 2
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif
if has("gui_running")
  set background=dark
  set t_Co=256
  set cursorline
  highlight CursorLine guibg=#003853 ctermbg=24 gui=none cterm=none
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")
  " always set autoindenting on
  set autoindent		
  augroup vimrcEx
  au!
  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78
  " colorcolumn
  set colorcolumn=80              " Show the 80th char column.
  augroup END
  filetype plugin indent on
  " Put these in an autocmd group, so that we can delete them easily.
    " Syntax of these languages is fussy over tabs Vs spaces
  autocmd FileType make setlocal ts=8 sts=8 sw=8 noexpandtab
  autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

  " Customisations based on house-style (arbitrary)
  autocmd FileType html setlocal ts=2 sts=2 sw=2 expandtab
  autocmd FileType css setlocal ts=2 sts=2 sw=2 expandtab
  autocmd FileType javascript setlocal ts=2 sts=2 sw=2 noexpandtab
  autocmd FileType mail setlocal formatoptions-=t

else
  " always set autoindenting on
  set autoindent
endif " has("autocmd")


" load matchit for better '%'
if has('syntax') && has('eval')
  packadd! matchit
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""	PLUGIN 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:lightline = {
\   'colorscheme': 'corvine',
\   'active': {
\     'left':[[ 'mode', 'paste' ],
\             [ 'readonly', 'filename', 'modified' ]]
\   },
\   'tab': {
\     'active': ['tabnum'],
\     'inactive': ['tabnum']
\ },
\   'component': {
\     'lineinfo': ' %3l:%-2v',
\   },
\}
let g:lightline.separator = {
\   'left': '', 'right': ''
\}
let g:lightline.subseparator = {
\   'left': '', 'right': ''
\}
let g:lightline.tabline = {
\   'left': [['buffers']],
\   'right': [['close'],['tabs']]
\}
let g:lightline.component_expand = {
\   'buffers': 'lightline#bufferline#buffers'
\}
let g:lightline.component_type = {
\   'buffers': 'tabsel'
\}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"" Bufferline 
" always display status line
set laststatus=2
" do not show mode
set noshowmode
" Show tabline
set showtabline=2  
" Don't use GUI tabline
set guioptions-=e  
"""""""""""""""""""

set t_Co=256
" Use a dark background
set background=dark
syntax on
colorscheme corvine
highlight ColorColumn ctermbg=237


"" indentation
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab


"" Natural splitting
set splitbelow
set splitright

"" numbering
set number
set relativenumber

"" path completition
set path+=**

"" cursor options
let &t_SI = "\<Esc>[5 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" optional reset cursor on start:
"augroup myCmds
"au!
"autocmd VimEnter * silent !echo -ne "\e[4 q"
"augroup END


"" key Mappings
nnoremap <silent><Leader><CR> :noh<CR>
nnoremap <silent><leader>w :call ToggleWrap()<CR>
nnoremap <silent><leader>c :w !detex \| wc -w <CR>
nnoremap <silent><leader>g :Goyo<CR>
nnoremap <silent><leader>s :setlocal spell<CR>
nnoremap <leader>f :Find 
"vnoremap <silent><leader>y :w !xsel -b<CR>
" saving as sudo
"cmap w!! w !sudo tee > /dev/null % | edit

" Toogle wrapping
function ToggleWrap()
 if (&wrap == 1)
   if (&linebreak == 0)
     set linebreak
   else
     set nowrap
   endif
 else
   set wrap
   set nolinebreak
 endif
endfunction

"" Netrw config
let g:netrw_banner = 0
let g:netwr_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_winsize = 20

" termbg
function! AdaptColorscheme()
  highlight clear CursorLine
  highlight Normal ctermbg=none
  highlight LineNr ctermbg=none
  highlight Folded ctermbg=none
  highlight NonText ctermbg=none
  highlight SpecialKey ctermbg=none
"  highlight VertSplit ctermbg=none
  highlight SignColumn ctermbg=none
endfunction
call AdaptColorscheme()

"" FZF ftw for non-tmux
command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)
" Fix swapfile warning while editing markdown from joplin
autocmd BufRead,BufNewFile /home/ariqfadlan/.config/*/tmp/* setlocal shortmess +=A
autocmd BufRead,BufNewFile /home/ariqfadlan/.config/*/tmp/* let g:lightline.component = {
			\ 'filename': "hehehe"
			\}
"
" LaTeX in Markdown Syntax
function! MathAndLiquid()
    "" Define certain regions
    " Block math. Look for $$[anything]$$
    syn region math start=/\$\$/ end=/\$\$/
    " inline math. Look for $[not $][anything]$
    syn match math_block '\$[^$].\{-}\$'

    " Liquid single line. Look for {%[anything]%}
    syn match liquid '{%.*%}'
    " Liquid multiline. Look for {%[anything]%}[anything]{%[anything]%}
    syn region highlight_block start='{% highlight .*%}' end='{%.*%}'
    " Fenced code blocks, used in GitHub Flavored Markdown (GFM)
    syn region highlight_block start='```' end='```'

    "" Actually highlight those regions.
    hi link math Statement
    hi link liquid Statement
    hi link highlight_block Function
    hi link math_block Function
endfunction
" Call everytime we open a Markdown file
autocmd BufRead,BufNewFile,BufEnter *.md,*.markdown call MathAndLiquid()

" LaTeX

let g:document_viewer = 'zathura'
"augroup filetype_tex
"    autocmd!
"    autocmd BufWritePost *.tex,*.latex silent! execute '!latexmk -silent -pdf %; latexmk -c'
"augroup END
" Open
function! OpenPDF()
    silent execute '!' . g:document_viewer . ' ' . expand('%:r') . '.pdf &'
endfunction
augroup view_tex_pdf
    autocmd!
    autocmd BufEnter *.tex,*.latex nnoremap <buffer><silent> <BS> :call OpenPDF()<CR>
augroup END
